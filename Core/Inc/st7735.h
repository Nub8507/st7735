/*
 * st7735.h
 *
 *  Created on: Oct 3, 2021
 *      Author: User
 */

#ifndef ST7735_H_
#define ST7735_H_

#include <sys/_stdint.h>
#include <utility>

class st7735 {
public:

	struct st7735_data{
		void *spi_handle;
		void *reset_port;
		uint16_t reset_pin;
		void *cs_port;
		uint16_t cs_pin;
		void *rs_port;
		uint16_t rs_pin;
		uint8_t x_size;
		uint8_t y_size;
		uint8_t* dma_interrupt;


		st7735_data(){
			spi_handle=nullptr;
			reset_port=nullptr;
			reset_pin=0;
			cs_port=nullptr;
			cs_pin=0;
			rs_port=nullptr;
			rs_pin=0;
			x_size=0;
			y_size=0;
			dma_interrupt=nullptr;
		}

		st7735_data(const st7735_data& src){
			spi_handle=src.spi_handle;
			reset_port=src.reset_port;
			reset_pin=src.reset_pin;
			cs_port=src.cs_port;
			cs_pin=src.cs_pin;
			rs_port=src.rs_port;
			rs_pin=src.rs_pin;
			x_size=src.x_size;
			y_size=src.y_size;
			dma_interrupt=src.dma_interrupt;
		}

		st7735_data(st7735_data &&src)noexcept:
				spi_handle(std::exchange(src.spi_handle, nullptr)),
				reset_port(std::exchange(src.reset_port, nullptr)),
				reset_pin(std::exchange(src.reset_pin, 0)),
				cs_port(std::exchange(src.cs_port, nullptr)),
				cs_pin(std::exchange(src.cs_pin, 0)),
				rs_port(std::exchange(src.rs_port, nullptr)),
				rs_pin(std::exchange(src.rs_pin, 0)),
				x_size(std::exchange(src.x_size, 0)),
				y_size(std::exchange(src.y_size, 0)),
				dma_interrupt(std::exchange(src.dma_interrupt, nullptr))
		{}

	};

	struct st7735_pos{
		uint16_t x;
		uint16_t y;
	};

	struct st7735_size{
		uint16_t w;
		uint16_t h;
	};


    st7735(const st7735_data data);
	virtual ~st7735() = default;

    static uint16_t color_form_rgb(uint8_t r,uint8_t g,uint8_t b);

    void init() const noexcept;

	void draw_pixel(st7735_pos pos, uint16_t color) const noexcept;
	void fill_rectangle(st7735_pos pos, st7735_size size, uint16_t color) const noexcept;
	void fill_screen(uint16_t color) const noexcept;
	void draw_image(st7735_pos pos, st7735_size size,const uint16_t* data) const noexcept;

	void check_finish() const noexcept;


private:

	enum st7735_cmd{
		st7735_cmd_NOP				=			0x00,
		st7735_cmd_SWRESET			=			0x01,
		st7735_cmd_RDDID			=			0x04,
		st7735_cmd_RDDST			=			0x09,
		st7735_cmd_SLPIN			=			0x10,
		st7735_cmd_SLPOUT			=			0x11,
		st7735_cmd_PTLON			=			0x12,
		st7735_cmd_NORON			=			0x13,
		st7735_cmd_INVOFF			=			0x20,
		st7735_cmd_INVON			=			0x21,
		st7735_cmd_DISPOFF			=			0x28,
		st7735_cmd_DISPON			=			0x29,
		st7735_cmd_CASET			=			0x2A,
		st7735_cmd_RASET			=			0x2B,
		st7735_cmd_RAMWR			=			0x2C,
		st7735_cmd_RAMRD			=			0x2E,
		st7735_cmd_PTLAR			=			0x30,
		st7735_cmd_MADCTL			=			0x36,
		st7735_cmd_COLMOD			=			0x3A,
		st7735_cmd_FRMCTR1			=			0xB1,
		st7735_cmd_FRMCTR2			=			0xB2,
		st7735_cmd_FRMCTR3			=			0xB3,
		st7735_cmd_INVCTR			=			0xB4,
		st7735_cmd_DISSET5			=			0xB6,
		st7735_cmd_PWCTR1			=			0xC0,
		st7735_cmd_PWCTR2			=			0xC1,
		st7735_cmd_PWCTR3			=			0xC2,
		st7735_cmd_PWCTR4			=			0xC3,
		st7735_cmd_PWCTR5			=			0xC4,
		st7735_cmd_VMCTR1			=			0xC5,
		st7735_cmd_RDID1			=			0xDA,
		st7735_cmd_RDID2			=			0xDB,
		st7735_cmd_RDID3			=			0xDC,
		st7735_cmd_RDID4			=			0xDD,
		st7735_cmd_PWCTR6			=			0xFC,
		st7735_cmd_GMCTRP1			=			0xE0,
		st7735_cmd_GMCTRN1			=			0xE1,
	};

	//
	const uint8_t timeout 	=	100;
	const uint32_t max_size;
	mutable bool ready;
	mutable uint8_t buf1[160*128*2];
	mutable uint8_t buf2[160*128*2];
	mutable uint8_t* curr_buf;
	//
    const st7735_data p_data;
	//
	void send_cmd(st7735_cmd cmd) const noexcept;
	void send_data(uint8_t* buf,const uint32_t size) const noexcept;
	void send_data(uint8_t data) const noexcept;
	void cs_hi() const noexcept;
	void cs_low() const noexcept;
	void rs_data() const noexcept;
	void rs_cmd() const noexcept;
	void reset_hi() const noexcept;
	void reset_low() const noexcept;
    void wait_spi_ready() const noexcept;
    void reset() const noexcept;
    void setCanvas(st7735_pos pos, st7735_size size) const noexcept;

};

#endif /* ST7735_H_ */
