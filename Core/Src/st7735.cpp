/*
 * st7735.cpp
 *
 *  Created on: Oct 3, 2021
 *      Author: User
 */

#include "st7735.h"

#include "main.h"

void st7735::init() const noexcept {
    //
	SPI1->CR1 |= SPI_CR1_SPE;
	//
    reset();
    HAL_Delay(10);
    //
    send_cmd(st7735_cmd_SWRESET);
    HAL_Delay(150);
    //
    send_cmd(st7735_cmd_SLPOUT);
    HAL_Delay(255);
    //
    send_cmd(st7735_cmd_FRMCTR1);
    curr_buf[0]=0x01;
    curr_buf[1]=0x2C;
    curr_buf[2]=0x2D;
    send_data(curr_buf,3);
    //
    send_cmd(st7735_cmd_FRMCTR2);
    curr_buf[0]=0x01;
    curr_buf[1]=0x2C;
    curr_buf[2]=0x2D;
    send_data(curr_buf,3);
    //
    send_cmd(st7735_cmd_FRMCTR3);
    curr_buf[0]=0x01;
    curr_buf[1]=0x2C;
    curr_buf[2]=0x2D;
    curr_buf[3]=0x01;
    curr_buf[4]=0x2C;
    curr_buf[5]=0x2D;
    send_data(curr_buf,6);
    //
    send_cmd(st7735_cmd_INVCTR);
    send_data(0x07);
    //
    send_cmd(st7735_cmd_PWCTR1);
    curr_buf[0]=0xA2;
    curr_buf[1]=0x02;
    curr_buf[2]=0x84;
    send_data(curr_buf,3);
    //
    send_cmd(st7735_cmd_PWCTR2);
    send_data(0xC5);
    //
    send_cmd(st7735_cmd_PWCTR3);
    curr_buf[0]=0x0A;
    curr_buf[1]=0x00;
    send_data(curr_buf,2);
    //
    send_cmd(st7735_cmd_PWCTR4);
    curr_buf[0]=0x8A;
    curr_buf[1]=0x2A;
    send_data(curr_buf,2);
    //
    send_cmd(st7735_cmd_PWCTR5);
    curr_buf[0]=0x8A;
    curr_buf[1]=0xEE;
    send_data(curr_buf,2);
    //
    send_cmd(st7735_cmd_VMCTR1);
    send_data(0x0E);
    //
    send_cmd(st7735_cmd_INVOFF);
    //
    send_cmd(st7735_cmd_MADCTL);
    send_data(0xC0);
    //
    send_cmd(st7735_cmd_COLMOD);
    send_data(0x05);
    //
    send_cmd(st7735_cmd_GMCTRP1);
    curr_buf[0]=0x02;
    curr_buf[1]=0x1C;
    curr_buf[2]=0x07;
    curr_buf[3]=0x12;
    curr_buf[4]=0x37;
    curr_buf[5]=0x32;
    curr_buf[6]=0x29;
    curr_buf[7]=0x2D;
    curr_buf[8]=0x29;
    curr_buf[9]=0x25;
    curr_buf[10]=0x2B;
    curr_buf[11]=0x39;
    curr_buf[12]=0x00;
    curr_buf[13]=0x01;
    curr_buf[14]=0x03;
    curr_buf[15]=0x10;
    send_data(curr_buf,16);
    //
    send_cmd(st7735_cmd_GMCTRN1);
    curr_buf[0]=0x03;
    curr_buf[1]=0x1D;
    curr_buf[2]=0x07;
    curr_buf[3]=0x06;
    curr_buf[4]=0x2E;
    curr_buf[5]=0x2C;
    curr_buf[6]=0x29;
    curr_buf[7]=0x2D;
    curr_buf[8]=0x2E;
    curr_buf[9]=0x2E;
    curr_buf[10]=0x37;
    curr_buf[11]=0x3F;
    curr_buf[12]=0x00;
    curr_buf[13]=0x00;
    curr_buf[14]=0x02;
    curr_buf[15]=0x10;
    send_data(curr_buf,16);
    //
    send_cmd(st7735_cmd_NORON);
    HAL_Delay(10);
    //
    send_cmd(st7735_cmd_DISPON);
    HAL_Delay(1000);
    //
}

void st7735::fill_screen(uint16_t color) const noexcept {
	//
	fill_rectangle({1,1},{p_data.x_size,p_data.y_size},color);
	//
}

st7735::st7735(const st7735::st7735_data data):
	max_size(data.x_size*data.y_size),
    p_data(data)

{
	curr_buf=buf1;
	ready=true;
}

void st7735::check_finish() const noexcept
{
	if(p_data.dma_interrupt!=nullptr){
		if(*p_data.dma_interrupt==1){
			*p_data.dma_interrupt=0;
			ready=true;
			cs_hi();
		}
	}
}

void st7735::send_cmd(st7735::st7735_cmd cmd) const noexcept
{
    //
	while(!ready)check_finish();
	//
    uint8_t cmd_8= static_cast<uint8_t>(cmd);
    //
    cs_low();
    //
    rs_cmd();
    HAL_SPI_Transmit(static_cast<SPI_HandleTypeDef *>(p_data.spi_handle), &cmd_8, sizeof(cmd_8), HAL_MAX_DELAY);
    //
    wait_spi_ready();
    cs_hi();
    //
}

void st7735::wait_spi_ready() const noexcept
{
    //
    while((SPI1->SR & SPI_SR_TXE) == RESET);
    while((SPI1->SR & SPI_SR_BSY) != RESET);
    //
}

void st7735::draw_pixel(st7735_pos pos, uint16_t color) const noexcept {
    //
    setCanvas(pos,{1,1});
    curr_buf[0]=color&0xFF>>8;
    curr_buf[1]=color&0xFF;
    send_data(curr_buf,2);
    //
}

void st7735::draw_image(st7735_pos pos, st7735_size size,const uint16_t* data) const noexcept {
    //
	setCanvas({pos.x-1,pos.y-1},{size.w-1,size.h-1});
	//
	auto data_size=size.h*size.w;
	for(int i=0;i<data_size;i++){
		curr_buf[2*i]=data[i]>>8;
		curr_buf[2*i+1]=data[i]&0xFF;
	}
	//
	send_data(curr_buf,data_size*2);
	//
}


void st7735::reset() const noexcept
{
    //
	reset_hi();
	HAL_Delay(20);
    reset_low();
    HAL_Delay(20);
    reset_hi();
    //
}

void st7735::send_data(uint8_t data) const noexcept
{
    //
	while(!ready)check_finish();
	//
	cs_low();
	//
    rs_data();
    HAL_SPI_Transmit(static_cast<SPI_HandleTypeDef *>(p_data.spi_handle), &data, sizeof(data), HAL_MAX_DELAY);
    //
    wait_spi_ready();
	cs_hi();
    //
}

void st7735::send_data(uint8_t* buf,const uint32_t size) const noexcept
{
	while(!ready)check_finish();
	//
	cs_low();
	//
    rs_data();
    //
    ready=false;
    HAL_SPI_Transmit_DMA(static_cast<SPI_HandleTypeDef *>(p_data.spi_handle), buf, size);
    if (curr_buf==buf1)curr_buf=buf2;
    else curr_buf=buf1;
    //}
    //HAL_SPI_Transmit(static_cast<SPI_HandleTypeDef *>(p_data.spi_handle), (uint8_t*)data.data(), data.size(), HAL_MAX_DELAY);
}

inline void st7735::cs_hi() const noexcept {
	HAL_GPIO_WritePin(static_cast<GPIO_TypeDef*>(p_data.cs_port), p_data.cs_pin, GPIO_PIN_SET);
}

inline void st7735::cs_low() const noexcept {
	HAL_GPIO_WritePin(static_cast<GPIO_TypeDef*>(p_data.cs_port), p_data.cs_pin, GPIO_PIN_RESET);
}

inline void st7735::rs_data() const noexcept {
	HAL_GPIO_WritePin(static_cast<GPIO_TypeDef*>(p_data.cs_port), p_data.rs_pin, GPIO_PIN_SET);
}

inline void st7735::rs_cmd() const noexcept {
	HAL_GPIO_WritePin(static_cast<GPIO_TypeDef*>(p_data.rs_port), p_data.rs_pin, GPIO_PIN_RESET);
}

inline void st7735::reset_hi() const noexcept {
	HAL_GPIO_WritePin(static_cast<GPIO_TypeDef*>(p_data.reset_port), p_data.reset_pin, GPIO_PIN_SET);
}

inline void st7735::reset_low() const noexcept {
	HAL_GPIO_WritePin(static_cast<GPIO_TypeDef*>(p_data.reset_port), p_data.reset_pin, GPIO_PIN_RESET);
}

void st7735::setCanvas(st7735_pos pos, st7735_size size) const noexcept {
	//
    // column address set
	send_cmd(st7735_cmd_CASET);
	curr_buf[0]=0x00;
	curr_buf[1]=pos.x;
	curr_buf[2]=0x00;
	curr_buf[3]=pos.x+size.h;
    send_data(curr_buf,4);

    // row address set
    send_cmd(st7735_cmd_RASET);
	curr_buf[1]=pos.y;
	curr_buf[3]=pos.y+size.w;
	send_data(curr_buf,4);

    // write to RAM
    send_cmd(st7735_cmd_RAMWR);
	//
}

uint16_t st7735::color_form_rgb(uint8_t r, uint8_t g, uint8_t b)
{
    return ((r & 0xf8) << 8) | ((g & 0xfc) << 3) | (b >> 3);
}

void st7735::fill_rectangle(st7735_pos pos, st7735_size size,
		uint16_t color) const noexcept {
	//
	setCanvas({pos.x-1,pos.y-1},{size.w-1,size.h-1});
	//
	auto data_size=size.h*size.w;
	for(int i=0;i<data_size;i++){
		curr_buf[2*i]=color>>8;
		curr_buf[2*i+1]=color&0xFF;
	}
	send_data(curr_buf,data_size*2);
}
