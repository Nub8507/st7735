#ifndef CONVERT_TO_TXT_H
#define CONVERT_TO_TXT_H

#include <QFile>
#include <QString>

class convert_to_txt
{
public:
    convert_to_txt(const QString f_name):file_name(f_name),out_file(nullptr){}

    void convert_file();

private:

    const QString file_name;
    QFile *out_file;
    void append_data(QString &str);

};

#endif // CONVERT_TO_TXT_H
