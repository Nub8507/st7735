#include "convert_to_txt.h"

#include <QCoreApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    bool skip_first=true;
    for(const auto &s:QCoreApplication::arguments()){
        if(skip_first){
            skip_first=false;
            continue;
        }
        auto t=convert_to_txt(s);
        t.convert_file();
    }

    return a.exec();
}
