#include "convert_to_txt.h"
#include <QCoreApplication>
#include <QImage>

void convert_to_txt::convert_file()
{
    QImage imj = QImage(QCoreApplication::instance()->applicationDirPath()+"/"+file_name);
    if(imj.isNull()){
        return;
    }
    imj=imj.convertToFormat(QImage::Format_RGB16);
    auto bits=imj.bits();
    auto size=imj.sizeInBytes();
    QString rez="{ ";
    for(auto i=0;i<size/2;i++){
        quint8 f1=bits[2*i];
        quint8 f2=bits[2*i+1];
        quint16 num=static_cast<quint16>(f2<<8)|f1;
        rez+="0x"+QString::number(num,16)+", ";
    }
    rez+="}\n\n";
    append_data(rez);
}

void convert_to_txt::append_data(QString &str)
{
    if(out_file==nullptr){
        out_file=new QFile("out_file.txt");
        if(!out_file->open(QIODevice::Append)){
            delete out_file;
            out_file=nullptr;
            return;
        }
    }
    //
    out_file->write(str.toLatin1());
    out_file->flush();
    //
}
